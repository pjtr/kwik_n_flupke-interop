FROM peterdoornbosch/quic-network-simulator-endpoint-with-jdk:jdk-21

# Get Kwik
COPY kwik-cli/bin bin
COPY kwik-cli/lib lib
COPY kwik-interop/bin bin
COPY kwik-interop/lib lib

# and Flupke
COPY flupke.jar .
COPY flupke-uber.jar .

# copy run script and run it
COPY run_endpoint.sh .
RUN chmod +x run_endpoint.sh
ENTRYPOINT [ "./run_endpoint.sh" ]
