#!/bin/bash

# Set up the routing needed for the simulation
/setup.sh

# The following variables are available for use:
# - ROLE contains the role of this execution context, client or server
# - SERVER_PARAMS contains user-supplied command line parameters
# - CLIENT_PARAMS contains user-supplied command line parameters

logDownloads() {
    echo "Downloaded files:"
    ls -l /downloads
}

createKeyStore() {
    openssl pkcs12 -in /certs/cert.pem -inkey /certs/priv.key -export -out /server.p12 -name servercert -password pass:secret
    keytool -importkeystore -deststorepass secret -destkeypass secret -destkeystore /server.keystore -srckeystore /server.p12 -srcstoretype PKCS12 -srcstorepass secret -alias servercert
}

createTrustStore() {
    keytool -importcert -keystore /cacerts.keystore -file /certs/ca.pem -alias Interop-Runner-Root-CA -storepass secret -noprompt
}

createInteropHttp3Script() {
    cp bin/kwik-interop bin/kwik-interop-with-flupke
    sed -i 's/CLASSPATH=$APP_HOME/CLASSPATH=flupke.jar:$APP_HOME/' bin/kwik-interop-with-flupke
}

if [ "$ROLE" == "client" ]; then
    createTrustStore
    
    # Wait for the simulator to start up.
    /wait-for-it.sh sim:57832 -s -t 30

    if [ "$TESTCASE" == "versionnegotiation" ]; then
        bin/kwik-cli -l n --reservedVersion $REQUESTS
    elif [ "$TESTCASE" == "handshake" ]; then
        echo "running kwik `bin/kwik-cli -v` with `java --version | head -1` with $REQUESTS"
        bin/kwik-cli -v1 -A hq-interop --trustStore cacerts.keystore --trustStorePassword secret -l wi -O /downloads $REQUESTS
    elif [ "$TESTCASE" == "retry" ]; then
        bin/kwik-cli -v1 -A hq-interop --trustStore cacerts.keystore --trustStorePassword secret -l wip -O /downloads $REQUESTS
    elif [ "$TESTCASE" == "resumption" ]; then
        bin/kwik-interop -c /downloads cacerts.keystore secret resumption $REQUESTS
    elif [ "$TESTCASE" == "transfer" ]; then
	bin/kwik-interop -c /downloads cacerts.keystore secret transfer $REQUESTS
    elif [ "$TESTCASE" == "multiconnect" ]; then
        bin/kwik-interop -c /downloads cacerts.keystore secret multiconnect $REQUESTS
    elif [ "$TESTCASE" == "http3" ]; then
	java -jar flupke-uber.jar
        java -ea -cp flupke-uber.jar tech.kwik.flupke.sample.AsyncHttp3 --disableCertificateCheck /downloads $REQUESTS
    elif [ "$TESTCASE" == "zerortt" ]; then
        bin/kwik-interop -c /downloads cacerts.keystore secret zerortt $REQUESTS
    elif [ "$TESTCASE" == "chacha20" ]; then
        bin/kwik-cli  -v1 -A hq-interop --chacha20 --trustStore cacerts.keystore --trustStorePassword secret -l wip -L /logs/kwik_client.log -O /downloads $REQUESTS
    elif [ "$TESTCASE" == "keyupdate" ]; then
        bin/kwik-interop -c /downloads cacerts.keystore secret keyupdate $REQUESTS
    elif [ "$TESTCASE" == "v2" ]; then
	bin/kwik-cli -v1v2 -A hq-interop --trustStore cacerts.keystore --trustStorePassword secret --secrets $SSLKEYLOGFILE -l wip -O /downloads $REQUESTS
    else
        echo "Unsupported testcase: ${TESTCASE}"
        exit 127
    fi
elif [ "$ROLE" == "server" ]; then
    createKeyStore
    createInteropHttp3Script
    if [ "$TESTCASE" == "handshake" ]; then
        echo "running kwik server version " `bin/kwik-interop -v`
        bin/kwik-interop -s --noRetry /server.keystore servercert secret secret 443 /www
    elif [[ "$TESTCASE" == "transfer" || "$TESTCASE" == "multiconnect" || "$TESTCASE" == "resumption" || "$TESTCASE" == "zerortt" || "$TESTCASE" == "chacha20" ]]; then
        bin/kwik-interop -s --noRetry /server.keystore servercert secret secret 443 /www
    elif [ "$TESTCASE" == "retry" ]; then
        bin/kwik-interop -s /server.keystore servercert secret secret 443 /www
    elif [ "$TESTCASE" == "http3" ]; then
	java -jar flupke.jar
	bin/kwik-interop-with-flupke -s --noRetry /server.keystore servercert secret secret 443 /www
    elif [ "$TESTCASE" == "v2" ]; then
        bin/kwik-interop -s --noRetry /server.keystore servercert secret secret  443 /www
    else
        echo "Unsupported testcase: ${TESTCASE}"
        exit 127
    fi
fi
